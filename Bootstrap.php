<?php

class Shopware_Plugins_Backend_KBKommunenAdministration_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    public function getVersion()
    {
        return '0.0.6';
    }

    public function getLabel()
    {
        return 'Kommunalbedarf Kommunenadministration';

    }

    public function getInfo()
    {
        return array(
            // Die Plugin-Version.
            'version' => $this->getVersion(),
            // Copyright-Hinweis
            'copyright' => 'Copyright (c) 2016, Martin Brack',
            // Lesbarer Name des Plugins
            'label' => $this->getLabel(),
            // Info-Text, der in den Plugin-Details angezeigt wird
            'link' => 'http://www.kommunalverlag.at',
            // Änderungen
            'changes' => array(
                '0.0.5' => array('releasedate' => '2016-06-03', 'lines' => array(
                    'Document_Attribute'
                ))
            ),
            // Aktuelle Revision des Plugins
            'revision' => '1'
        );
    }

    public function install()
    {
        $item = $this->createMenuItem(array(
            'label' => 'Kommunen',
            'class' => 'sprite-application-block',
            'active' => 1,
            'action' => 'Index',
            'controller' => 'KommunenAdministration',
            'parent' => $this->Menu()->findOneBy('label', 'Kunden'),
        ));

        try {
            $this->subscribeEvents();
            $this->createAttributes();
            $this->createDatabaseTables();
        } catch (Exception $e) {
            return array(
                'success' => false,
                'message' => $e->getMessage()
            );
        }
        return true;
    }

    public function update()
    {
        return true;
    }

    public function uninstall()
    {
        try {
            $this->removeAttributes();
            $this->removeDatabaseTables();
        } catch (\Exception $e) {
            // noting to do here.
        }

        return true;
    }

    private function subscribeEvents()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_KommunenAdministration',
            'onGetControllerPathBackend'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PreDispatch_Frontend_Checkout',
            'onPreDispatchCheckout'
        );
        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout',
            'onPostDispatchCheckout'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Customer',
            'onBackendCustomerPostDispatch'
        );

        $this->subscribeEvent(
            'Shopware\Models\Customer\Repository::getListQueryBuilder::after',
            'afterGetListQueryBuilder'
        );
    }

    public function afterInit()
    {
        $this->registerCustomModels();
    }

    public function onGetControllerPathBackend(Enlight_Event_EventArgs $args)
    {
        $this->Application()->Template()->addTemplateDir(
            $this->Path() . 'Views/'
        );

       
        

       
        return $this->Path() . '/Controllers/Backend/KommunenAdministration.php';
    }

    private function createDatabaseTables()
    {
        $modelManager = Shopware()->Models();

        $schemaTool = new Doctrine\ORM\Tools\SchemaTool($modelManager);
        $schemaTool->createSchema(
            array(
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Kommune'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Bonus'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Department'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Distributors\Distributor'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Contracts\Contract'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Contracts\Rule'),
            )
        );

        require_once('demodata.php'); 
        
    }

    private function removeDatabaseTables()
    {
        $modelManager = Shopware()->Models();

        $schemaTool = new Doctrine\ORM\Tools\SchemaTool($modelManager);
        $schemaTool->dropSchema(
            array(
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Kommune'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Bonus'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Kommunen\Department'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Distributors\Distributor'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Contracts\Contract'),
                $modelManager->getClassMetadata('Shopware\CustomModels\Contracts\Rule')
            )
        );
    }

    private function createAttributes()
    {

        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'k_id',
            'int(8)',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'customer_budget',
            'int(11)',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'user_manager',
            'boolean',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'confirming_orders',
            'boolean',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'department',
            'boolean',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'active',
            'boolean',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_user_attributes',
            'kb',
            'supervisor',
            'boolean',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_articles_attributes',
            'kb',
            'distributor',
            'int(8)',
            true,
            null
        );
        $this->Application()->Models()->addAttribute(
            's_order_attributes',
            'kb',
            'bonus',
            'int(8)',
            true,
            null
        );;

        $this->getEntityManager()->generateAttributeModels(array(
            's_user_attributes',
            's_articles_attributes',
            's_order_attributes'
        ));
    }

    private function removeAttributes()
    {
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'customer_budget'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'k_id'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'user_manager'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'confirming_orders'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'active'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'department'
        );
        $this->Application()->Models()->removeAttribute(
            's_user_attributes',
            'kb',
            'supervisor'
        );
        $this->Application()->Models()->removeAttribute(
            's_articles_attributes',
            'kb',
            'distibutor'
        );
        $this->Application()->Models()->removeAttribute(
            's_order_attributes',
            'kb',
            'bonus'
        );
        $this->getEntityManager()->generateAttributeModels(array(
            's_user_attributes',
            's_articles_attributes',
            's_order_attributes'
        ));


    }

    /**
     * @return \Shopware\Components\Model\ModelManager
     */
    protected function getEntityManager()
    {
        return Shopware()->Models();
    }

    /**
     * Event listener function which called over the Enlight_Controller_Action_PreDispatch_Frontend_Checkout event.
     * The event fired when the customer is about to finish his checkout, before the actual dispatch.
     *
     * @param Enlight_Event_EventArgs $arguments
     */
    public function onPreDispatchCheckout(Enlight_Event_EventArgs $arguments)
    {
        /**@var $subject Shopware_Controllers_Frontend_Checkout */
        $subject = $arguments->getSubject();
        $request = $subject->Request();
        $action = $request->getActionName();

        $customerId = Shopware()->Session()->sUserId;
        if ($action !== "finish" || $customerId === null) {
            return;
        }

        // Get customer by Id
        /** @var $customer \Shopware\Models\Customer\Customer */
        $customer = Shopware()->Models()->find('Shopware\Models\Customer\Customer', $customerId);
        if (!$customer) {
            return;
        }

        $message = $this->checkBudget($customer);

        // forward to confirmAction and pass the message
        /*if ($message !== true) {
            $subject->forward('confirm', 'checkout', 'frontend', array('sBudgetInfo' => $message));
        }*/
    }

    /**
     * Helper function which checks if budget is enough
     *
     * Returns true if he is or an error message if he is not
     *
     * @param \Shopware\Models\Customer\Customer $customer
     * @return null|string
     */
    private function checkBudget($customer)
    {
        $namespace = Shopware()->Snippets()->getNamespace('frontend/checkout/check_budget');

        $budget = $customer->getAttribute()->getKBCustomerBudget();
        $order = Shopware()->Session()->sBasketAmount;
        $basket = Shopware()->Modules()->Basket()->sGetBasket();

        if (!$budget) {
            return $namespace->get(
                'budget_required',
                'Für Ihr Benutzerkonto wurde kein Budget hinterlegt.',
                true
            );
        }

        // Return message if the user is not old enough
        if ($budget < $order) {
            $snippet = $namespace->get(
                'no_budget',
                "Ihr Budget von %s € reicht für diese Bestellung nicht aus.",
                true
            );
            return sprintf($snippet, $budget);
        }

        return true;
    }

    /**
     * Event listener function which called over the Enlight_Controller_Action_PostDispatch_Frontend_Checkout event.
     * The event fired when the customer is about to finish his checkout, after the actual dispatch.
     *
     * @param Enlight_Event_EventArgs $arguments
     */
    public function onPostDispatchCheckout(Enlight_Event_EventArgs $arguments)
    {
        /**@var $subject Shopware_Controllers_Frontend_Checkout */
        $subject = $arguments->getSubject();
        $request = $subject->Request();
        $response = $subject->Response();
        $action = $request->getActionName();
        $view = $subject->View();

        if ($action !== 'confirm' || !$request->isDispatched() ||
            $response->isException() || !$view->hasTemplate()
        ) {
            return;
        }

        // Check if the param sBudgetInfo was set by our preDispatch method.
        // If this is the case, set sBasketInfo
        if ($request->has('sBudgetInfo')) {
            $view->assign('sBasketInfo', $request->getParam('sBudgetInfo'));
        }
    }

    /**
     * Hooks the getListQueryBuilder method of the customer repository.
     * Additionally selects our own attributes
     *
     * @param Enlight_Hook_HookArgs $arguments
     */
    public function afterGetListQueryBuilder(Enlight_Hook_HookArgs $arguments)
    {
        // get original builder
        $builder = $arguments->getReturn();

        // add selection and join to original builder
        $builder->addSelect(array(
            'attributes.kbKId',
            'attributes.kbCustomerBudget',
            'attributes.kbUserManager',
            'attributes.kbConfirmingOrders',
        ));
        $builder->leftJoin('customer.attribute', 'attributes');

        $arguments->setReturn($builder);
    }


    /**
     * Called when the BackendCustomerPostDispatch Event is triggered
     *
     * @param Enlight_Event_EventArgs $args
     */
    public function onBackendCustomerPostDispatch(Enlight_Event_EventArgs $args)
    {
        /**@var $view Enlight_View_Default */
        $view = $args->getSubject()->View();

        // Add snippet directory
        $this->Application()->Snippets()->addConfigDir(
            $this->Path() . 'Snippets/'
        );

        // Add template directory
        $args->getSubject()->View()->addTemplateDir(
            $this->Path() . 'Views/'
        );

        //if the controller action name equals "load" we have to load all application components
        if ($args->getRequest()->getActionName() === 'load') {
            $view->extendsTemplate(
                'backend/customer/model/check_budget/attribute.js'
            );
            $view->extendsTemplate(
                'backend/customer/model/check_budget/list.js'
            );
            $view->extendsTemplate(
                'backend/customer/view/list/check_budget/list.js'
            );
            $view->extendsTemplate(
                'backend/customer/view/detail/check_budget/window.js'
            );
        }

        //if the controller action name equals "index" we have to extend the backend customer application
        if ($args->getRequest()->getActionName() === 'index') {
            $view->extendsTemplate('backend/customer/check_budget_app.js');
        }
    }
}

?>