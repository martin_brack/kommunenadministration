<?php
use Shopware\CustomModels\Kommunen\Kommune;
use Shopware\CustomModels\Kommunen\Bonus;

class Shopware_Controllers_Backend_KommunenAdministration extends Shopware_Controllers_Backend_Application
    {
        protected $model = 'Shopware\CustomModels\Kommunen\Kommune';
        protected $alias = 'kommune';


        protected function getDetailQuery($id)
        {
            $builder = parent::getDetailQuery($id);

            $builder->leftJoin('kommune.bonus', 'bonus');
            $builder->addSelect(array('bonus'));

            return $builder;
        }

        protected function getAdditionalDetailData(array $data)
        {
            $data['bonus'] = array();
            return $data;
        }
    }
?>