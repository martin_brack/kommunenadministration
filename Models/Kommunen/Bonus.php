<?php

namespace   Shopware\CustomModels\Kommunen;


use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="kb_bonus")
 */
class Bonus extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="kommune_id", type="integer")
     */
    protected $kommuneId;

    /**
     *
     * @ORM\ManyToOne(
     *      targetEntity="Kommune",
     *      inversedBy="bonus",
     * )
     *  @ORM\JoinColumn(name="kommune_id", referencedColumnName="id")
     */
    protected $kommune;


    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(name="datum", type="date")
     */
    private $datum;

    /**
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $label;

    /**
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var Order
     * @ORM\OneToOne(
     *      targetEntity="\Shopware\Models\Order\Order",
     * )
     * @ORM\JoinColumn(name="order_id", referencedColumnName="ordernumber")
     */
    private $order;

    /**
     * @ORM\Column(name="order_id", type="integer", nullable=true)
     */
    private $orderId;

    /**
     * @return mixed
     */
    public function getKommune()
    {
        return $this->kommune;
    }

    /**
     * @param mixed $kommune
     */
    public function setKommune($kommune)
    {
        $this->kommune = $kommune;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }


    /**
     * Event listener method which fired when the model will be saved.
     * Initials the date time fields if this fields are null.
     * @ORM\PrePersist
     */
    public function onSave()
    {

    }

    /**
     * Event listener method which fired when the model will be updated.
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {

    }

}
