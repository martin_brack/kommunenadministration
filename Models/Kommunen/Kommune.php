<?php

namespace   Shopware\CustomModels\Kommunen;

use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
//use Shopware\CustomModels\Distributors;
//use Shopware\CustomModels\Contracts;

/**
 * @ORM\Entity
 * @ORM\Table(name="kb_kommunen")
 */
class Kommune extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $zip
     *
     * @ORM\Column(name="zip", type="integer", nullable=false)
     */
    private $zip = 0;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", length=30, nullable=false)
     */
    private $label = '';

    /**
     * @var Bonus[]
     * @ORM\OneToMany(
     *      targetEntity="Bonus",
     *      mappedBy="kommune",
     *      cascade={"persist", "remove"}
     *     )
     */
    protected $bonus;

    /**
     * @var Contract[]
     * @ORM\OneToMany(
     *      targetEntity="\Shopware\CustomModels\Contracts\Contract",
     *      mappedBy="kommune",
     *      cascade={"persist", "remove"}
     *     )
     */
    protected $contracts;

    /**
     * @var Customerattributes[]
     * @ORM\ManyToOne(
     *      targetEntity="\Shopware\Models\Attribute\Customer",
     *      inversedBy="kb_k_id"
     *     )
     */
    protected $customers;

    /**
     * @return \Shopware\CustomModels\Contracts\ Contract[]
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @param \Shopware\CustomModels\Contracts\Contract[]
     * @return \Shopware\Components\Model\ModelEntity
     */
    public function setContracts($contracts)
    {
        return $this->setOneToMany(
            $contracts,
            '\Shopware\CustomModels\Contracts\Contract',
            'contracts',
            'kommune'
        );
    }

    /**
     * @return \Shopware\CustomModels\Kommunen\ Bonus[]
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param \Shopware\CustomModels\Kommunen\Bonus[] $Bonus
     * @return \Shopware\Components\Model\ModelEntity
     */
    public function setBonus($bonus)
    {
        return $this->setOneToMany(
            $bonus,
            '\Shopware\CustomModels\Kommunen\Bonus',
            'bonus',
            'kommune'
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Customerattributes[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @param Customerattributes[] $customers
     */
    public function setCustomers($customers)
    {
        $this->customers = $customers;
    }



    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }



    public function __construct()
    {
        $this->bonus = new ArrayCollection();
        $this->contracts = new ArrayCollection();
    }

}
