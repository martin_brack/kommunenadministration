<?php

namespace   Shopware\CustomModels\Contracts;

use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="kb_rules")
 */
class Rule extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @ORM\Column(name="contract_id", type="integer")
     */
    protected $contractId;

    /**
     *
     * @ORM\ManyToOne(
     *      targetEntity="\Shopware\CustomModels\Contracts\Contract",
     *      inversedBy="rules",
     * )
     *  @ORM\JoinColumn(name="contract_id", referencedColumnName="id")
     */
    protected $contract;

    /**
     * @var integer $active
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = 0;

    /**
     * @var integer $type
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type = 0;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", nullable=true)
     */
    private $label = '';

    /**
     * @var integer $value
     *
     * @ORM\Column(name="value", type="integer", nullable=true)
     */
    private $value = 0;

    /**
     * @var date $created
     *
     * @ORM\Column(name="created", type="date", nullable=false)
     */
    private $created = 0;

    /**
     * @var date $last_edit
     *
     * @ORM\Column(name="last_edit", type="date", nullable=false)
     */
    private $last_edit = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getContractId()
    {
        return $this->contractId;
    }

    /**
     * @param mixed $contractId
     */
    public function setContractId($contractId)
    {
        $this->contractId = $contractId;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return date
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return date
     */
    public function getLastEdit()
    {
        return $this->last_edit;
    }

    /**
     * @param date $last_edit
     */
    public function setLastEdit($last_edit)
    {
        $this->last_edit = $last_edit;
    }


}
