<?php

namespace   Shopware\CustomModels\Contracts;

use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="kb_contracts")
 */
class Contract extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="kommune_id", type="integer")
     */
    protected $kommuneId;

    /**
     *
     * @ORM\ManyToOne(
     *      targetEntity="\Shopware\CustomModels\Kommunen\Kommune",
     *      inversedBy="contracts",
     * )
     *  @ORM\JoinColumn(name="kommune_id", referencedColumnName="id")
     */
    protected $kommune;

    /**
     * @ORM\Column(name="distributor_id", type="integer")
     */
    protected $distributorId;

    /**
     *
     * @ORM\ManyToOne(
     *      targetEntity="\Shopware\CustomModels\Distributors\Distributor",
     *      inversedBy="contracts",
     * )
     *  @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     */
    protected $distributor;

    /**
     * @var Rule[]
     * @ORM\OneToMany(
     *      targetEntity="\Shopware\CustomModels\Contracts\Rule",
     *      mappedBy="contract",
     *      cascade={"persist", "remove"}
     *     )
     */
    protected $rules;

    /**
     * @return \Shopware\CustomModels\Contracts\Rule[]
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param \Shopware\CustomModels\Contracts\Rule[]
     * @return \Shopware\Components\Model\ModelEntity
     */
    public function setRules($rules)
    {
        return $this->setOneToMany(
            $rules,
            '\Shopware\CustomModels\Contracts\Rule',
            'rules',
            'contract'
        );
    }

    /**
     * @var string $active
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = 0;

    /**
     * @var date $created
     *
     * @ORM\Column(name="created", type="date", nullable=false)
     */
    private $created = 0;

    /**
     * @var date $last_edit
     *
     * @ORM\Column(name="last_edit", type="date", nullable=false)
     */
    private $last_edit = 0;

    public function __construct()
    {
        $this->rules = new ArrayCollection();

    }

}
