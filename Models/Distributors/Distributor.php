<?php

namespace   Shopware\CustomModels\Distributors;

use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="kb_distributors")
 */
class Distributor extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name = '';

    /**
     * @var string $strasse
     *
     * @ORM\Column(name="street", type="string", length=30, nullable=false)
     */
    private $street = '';

    /**
     * @var integer $zip
     *
     * @ORM\Column(name="zip", type="integer", nullable=false)
     */
    private $zip = 0;

    /**
     * @var string $ort
     *
     * @ORM\Column(name="city", type="string", length=30, nullable=false)
     */
    private $city = '';

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=30, nullable=false)
     */
    private $url = '';

    /**
     * @var string $email
     *
     * @ORM\Column(name="orderemail", type="string", length=30, nullable=false)
     */
    private $orderemail = '';

    /**
     * @var Contract[]
     * @ORM\OneToMany(
     *      targetEntity="\Shopware\CustomModels\Contracts\Contract",
     *      mappedBy="distributor",
     *      cascade={"persist", "remove"}
     *     )
     */
    protected $contracts;

    /**
     * @return \Shopware\CustomModels\Contracts\ Contract[]
     */
    public function getContracts()
    {
        return $this->contracts;
    }

    /**
     * @param \Shopware\CustomModels\Contracts\Contract[]
     * @return \Shopware\Components\Model\ModelEntity
     */
    public function setContracts($contracts)
    {
        return $this->setOneToMany(
            $contracts,
            '\Shopware\CustomModels\Contracts\Contract',
            'contracts',
            'distributor'
        );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return int
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param int $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getOrderemail()
    {
        return $this->orderemail;
    }

    /**
     * @param string $orderemail
     */
    public function setOrderemail($orderemail)
    {
        $this->orderemail = $orderemail;
    }



    public function __construct()
    {
        $this->contracts = new ArrayCollection();
    }

}
