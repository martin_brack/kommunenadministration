Ext.define('Shopware.apps.KommunenAdministration.store.Bonus', {
    extend: 'Shopware.store.Association',
    model: 'Shopware.apps.KommunenAdministration.model.Bonus',
    configure: function() {
        return {
            controller: 'KommunenAdministration'
        };
    }
});

