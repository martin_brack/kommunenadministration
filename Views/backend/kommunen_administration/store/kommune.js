Ext.define('Shopware.apps.KommunenAdministration.store.Kommune', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'KommunenAdministration'
        };
    },
    model: 'Shopware.apps.KommunenAdministration.model.Kommune'
});
