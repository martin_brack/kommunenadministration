Ext.define('Shopware.apps.KommunenAdministration.view.list.Kommune', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.kommune-listing-grid',
    region: 'center',
    configure: function() {
        return {
            detailWindow: 'Shopware.apps.KommunenAdministration.view.detail.Window',
            columns: {
                zip: { header: 'PLZ', flex:1},
                label: { header:'Ort', flex:6}
            },

        };
    }
});
