Ext.define('Shopware.apps.KommunenAdministration.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.kommune-list-window',
    height: 515,
    width: 350,
    title : '{s name=window_title}Kommunen: Administration{/s}',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.KommunenAdministration.view.list.Kommune',
            listingStore: 'Shopware.apps.KommunenAdministration.store.Kommune'
        };
    }
});
