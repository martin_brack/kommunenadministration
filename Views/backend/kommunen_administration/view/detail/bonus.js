Ext.define('Shopware.apps.KommunenAdministration.view.detail.Bonus', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.shopware-kommune-bonus-grid',
    title: 'Bonus',
    height: 250,
    configure: function() {
        return {
            detailWindow: 'Shopware.apps.KommunenAdministration.view.detail.Bonuswindow',
            columns: {
                datum: { header: 'Datum', flex:2},
                label: { header: 'Bezeichnung', flex:3},
                active: { header:'Aktiv', flex:1},
                value: { header:'Wert', flex:1}
            },

        };
    }
});
