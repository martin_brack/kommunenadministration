Ext.define('Shopware.apps.KommunenAdministration.view.detail.Kommune', {
    extend: 'Shopware.model.Container',
    alias: 'widget.kommune-detail-container',
    padding: 20,
    configure: function() {
        return {
            controller: 'KommunenAdministration',
            associations: [ 'bonus' ],
            fieldSets: [
                {
                    fields: {
                        zip: { fieldLabel: 'PLZ', disabled:true},
                        label: { fieldLabel: 'Ort', disabled:true},
                    },
                    title: undefined
                }
            ],
        };
    }
});
