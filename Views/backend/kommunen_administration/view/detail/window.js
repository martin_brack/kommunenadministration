Ext.define('Shopware.apps.KommunenAdministration.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.kommune-detail-window',
    title : '{s name=title}Kommune: Details{/s}',
    height: 420,
    width: 900,
    configure: function() {
        return {
            //associations: [ 'bonus']
        }
    }
   });
