
Ext.define('Shopware.apps.KommunenAdministration.view.detail.Bonuswindow', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.kommune-detail-bonuswindow',
    title : '{s name=bonus_detail_title}Bonus: Details{/s}',
    height: 420,
    width: 900,
    configure: function() {
        return {
            //associations: [ 'bonus']
        }
    }
});