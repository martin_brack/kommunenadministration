Ext.define('Shopware.apps.KommunenAdministration', {
    extend: 'Enlight.app.SubApplication',

    name:'Shopware.apps.KommunenAdministration',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.Kommune',

        'detail.Window',
        'detail.Kommune',
        'detail.Bonus',
    ],

    models: [ 'Kommune', 'Bonus'],
    stores: [ 'Kommune', 'Bonus'],

    launch: function() {
        return this.getController('Main').mainWindow;
    }

});
