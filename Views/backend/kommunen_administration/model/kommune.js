Ext.define('Shopware.apps.KommunenAdministration.model.Kommune', {
    extend: 'Shopware.data.Model',

    configure: function() {
        return {
            controller: 'KommunenAdministration',
            detail: 'Shopware.apps.KommunenAdministration.view.detail.Kommune'
        };
    },

    fields: [
        { name : 'id', type: 'int' },
        { name : 'zip', type: 'int' },
        { name : 'label', type: 'string' }
    ],
    associations: [ {
        relation: 'OneToMany',
        storeClass: 'Shopware.apps.KommunenAdministration.store.Bonus',
        loadOnDemand: true,
        model: 'Shopware.apps.KommunenAdministration.model.Bonus',
        type: 'hasMany',
        associationKey: 'bonus',
        name: 'getBonus'
        },
    ]
});



