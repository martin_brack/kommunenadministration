Ext.define('Shopware.apps.KommunenAdministration.model.Bonus', {
    extend: 'Shopware.data.Model',

    configure: function() {
        return {
            controller: 'KommunenAdministration',
            listing: 'Shopware.apps.KommunenAdministration.view.detail.Bonus',
            detail: 'Shopware.apps.KommunenAdministration.view.detail.Bonusdetail'
        };
    },

    fields: [
        { name: 'id', type: 'int' },
        //{ name: 'kommuneId', type: 'int' },
        { name: 'active', type: 'boolean' },
        { name: 'datum', type: 'date' },
        { name: 'label', type: 'string' },
        { name: 'value', type: 'int' }
    ]
});
