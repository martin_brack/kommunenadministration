
Ext.define('Shopware.apps.Customer.store.Kommune', {
    extend: 'Shopware.store.Listing',
    model: 'Shopware.apps.Customer.model.Kommune',
    configure: function() {
        return {
            controller: 'KommunenAdministration'
        };
    }
});
