//{block name="backend/customer/view/list/list" append}
//{namespace name=backend/kbCheckBudget/main}
Ext.define('Shopware.apps.Customer.view.list.List.check_budget.List', {

    /**
     * Defines an override applied to a class.
     * @string
     */
    override: 'Shopware.apps.Customer.view.list.List',

    /**
     * Overrides the getColumns function of the overridden ExtJs object
     * and inserts a column with the customers budget
     * @return
     */
    getColumns: function() {
        var me = this;

        var columns = me.callParent(arguments);

        var columnBudget= {
            header: '{s name=preferences/budget}Budget{/s}',
            dataIndex:'kbCustomerBudget',
            flex: 1
        };


        return Ext.Array.insert(columns, 8, [columnBudget]);
    }
});
//{/block}