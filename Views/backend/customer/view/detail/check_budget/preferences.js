//{namespace name=backend/kbCheckBudget/main}
Ext.define('Shopware.apps.Customer.view.detail.check_budget.Preferences', {
    /**
     * Define that the base field set is an extension of the Ext.form.FieldSet
     * @string
     */
    extend:'Ext.form.FieldSet',

    /**
     * Set css class for this component
     * @string
     */
    cls: Ext.baseCSSPrefix + 'preferences-field-set',

    /**
     * Layout type for the component.
     * @string
     */
    layout: 'column',

    /**
     * Component event method which is fired when the component
     * is initials. The component is initials when the user
     * want to create a new customer or edit an existing customer
     * @return void
     */
    initComponent:function () {
        var me = this;
        me.title = '{s name=preferences/title}Kommunalrechte{/s}';

        me.items = me.createForm();
        me.callParent(arguments);
    },


    /**
     * Creates both containers for the field set
     * to display the form fields in two columns.
     *
     * @return Array Contains the left and right container
     */
    createForm:function () {
        var leftContainer, rightContainer, me = this;

        leftContainer = Ext.create('Ext.container.Container', {
            columnWidth:0.5,
            border:false,
            cls: Ext.baseCSSPrefix + 'field-set-container',
            layout:'anchor',
            items:me.createFormLeft()
        });

        rightContainer = Ext.create('Ext.container.Container', {
            columnWidth:0.5,
            border:false,
            layout:'anchor',
            cls: Ext.baseCSSPrefix + 'field-set-container',
            items: me.createFormRight()
        });


        return [ leftContainer, rightContainer];
    },

    /**
     * Creates the left container of the base field set.
     *
     * @return Array Contains the different form field of the left container
     */
    createFormLeft:function () {
        var me = this;

        me.budget = Ext.create('Ext.form.field.Number', {
            name:'attribute[kbCustomerBudget]',
            fieldLabel:'{s name=preferences/budget}Budget{/s}',
            minValue: 1,
            allowBlank: true,
            anchor:'95%',
            labelWidth:150,
            minWidth:250
        });


        return [
            me.budget
        ];
    },
    createFormRight:function () {
        var me = this;

        me.userManager = Ext.create('Ext.form.field.Checkbox', {
            name:'attribute[kbUserManager]',
            fieldLabel:'{s name=preferences/userManager}Benutzerverwaltung{/s}',
            anchor:'95%',
            labelWidth:150,
            inputValue: 1,
            uncheckedValue: 0,
        });
        me.confirmingOrders = Ext.create('Ext.form.field.Checkbox', {
            name:'attribute[kbConfirmingOrders]',
            fieldLabel:'{s name=preferences/confirmingOrders}Bestellungen freigeben{/s}',
            anchor:'95%',
            labelWidth:150,
            inputValue: 1,
            uncheckedValue: 0,
        });

        return [
            me.userManager,
            me.confirmingOrders
        ];
    }


});