//{block name="backend/customer/model/attribute/fields" append}
{ name: 'kbKId', type: 'int', useNull:true  },
{ name: 'kbCustomerBudget', type: 'int', useNull:true  },
{ name: 'kbUserManager', type: 'boolean', useNull:true},
{ name: 'kbConfirmingOrders', type: 'boolean', useNull:true},
{ name: 'kbActive', type: 'boolean', useNull:true},
//{/block}